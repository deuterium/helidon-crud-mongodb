package cloud.deuterium.helidon.model;

/**
 * Created by Milan Stojkovic 24-Apr-2021
 */
public class ErrorResponse {
    public final int status;
    public final String message;
    public final String type;
    public final String timestamp;

    public ErrorResponse(int status, String message, String type, String timestamp) {
        this.status = status;
        this.message = message;
        this.type = type;
        this.timestamp = timestamp;
    }
}
