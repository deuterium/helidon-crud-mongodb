
package cloud.deuterium.helidon;

import cloud.deuterium.helidon.model.Atom;
import cloud.deuterium.helidon.model.ErrorResponse;
import io.helidon.common.http.Http;
import io.helidon.dbclient.DbClient;
import io.helidon.webserver.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * https://hantsy.medium.com/accessing-database-with-helidon-db-client-87aedacaaf43
 * https://github.com/tomas-langer/helidon-db-client-example
 *
 */

public class AtomService implements Service {

    private static final Logger LOGGER = Logger.getLogger(AtomService.class.getName());
    private final DbClient dbClient;

    AtomService(DbClient dbClient) {
        this.dbClient = dbClient;
    }

    /**
     * A service registers itself by updating the routing rules.
     *
     * @param rules the routing rules.
     */
    @Override
    public void update(Routing.Rules rules) {
        rules
             .get("/", this::getAtomsHandler)
             .get("/{atomNumber}", this::getAtomHandler)
             .post("/", Handler.create(Atom.class, this::postAtomHandler))
             .put("/{atomNumber}", Handler.create(Atom.class, this::updateAtomHandler))
             .delete("/{atomNumber}", this::deleteAtomHandler);
    }

    /**
     * Return all Atoms from the storage.
     *
     * @param request  the server request
     * @param response the server response
     */
    private void getAtomsHandler(ServerRequest request, ServerResponse response) {
        LOGGER.log(Level.INFO, "Returning All Atoms from storage");

        String query = "{" +
                "\"collection\": \"atoms\"," +
                "\"query\":{}" +
                "}";

        dbClient.execute(exec -> exec.query(query))
                .collectList()
                .thenAccept(rows -> {
                    List<Atom> atoms = rows.stream()
                            .map(row -> row.as(Atom.class))
                            .collect(Collectors.toList());
                    System.out.println(atoms);
                    response.status(Http.Status.OK_200).send(atoms);
                })
                .exceptionally(ex -> processErrors(ex, request, response));
    }


    /**
     * Return single Atom from storage.
     *
     * @param request  the server request
     * @param response the server response
     */
    private void getAtomHandler(ServerRequest request, ServerResponse response) {

        String query = "{" +
                "\"collection\": \"atoms\"," +
                "\"query\":{atomNumber:$atomNumber}" +
                "}";

        String aNumber = request.path().param("atomNumber");
        int atomNumber = Integer.parseInt(aNumber);

        dbClient.execute(exec -> exec.createGet(query)
                .addParam("atomNumber", atomNumber)
                .execute())
                .thenAccept(row -> {
                    Optional<Atom> optional = row.map(r -> r.as(Atom.class));
                    optional.ifPresentOrElse(
                            atom -> response.status(Http.Status.OK_200).send(atom),
                            () -> sendNotFoundError(response, aNumber));
                })
                .exceptionally(ex -> processErrors(ex, request, response));
    }

    private void sendNotFoundError(ServerResponse response, String atomNumber) {
        LOGGER.log(Level.WARNING, "Not found Atom with number: " + atomNumber);
        ErrorResponse error = new ErrorResponse(404,
                "Not found Atom with atom number: " + atomNumber,
                "NOT_FOUND",
                LocalDateTime.now().toString());
        response.status(Http.Status.NOT_FOUND_404).send(error);
    }

    private void sendBadRequestError(ServerResponse response, String message) {
        LOGGER.log(Level.WARNING, "Bad request: {}", message);
        ErrorResponse error = new ErrorResponse(400,
                message,
                "BAD_REQUEST",
                LocalDateTime.now().toString());
        response.status(Http.Status.BAD_REQUEST_400).send(error);
    }

    /**
     * Save new Atom to storage.
     *
     * @param request  the server request
     * @param response the server response
     * @param atom     the Atom object from request body
     */
    private void postAtomHandler(ServerRequest request, ServerResponse response, Atom atom) {

        String query = "{" +
                "\"collection\": \"atoms\"," +
                "\"value\":{" +
                "\"atomNumber\": $atomNumber," +
                "\"name\": $name," +
                "\"symbol\": $symbol," +
                "\"mass\": $mass" +
                "}" +
                "}";

        dbClient.execute(exec -> exec.createInsert(query)
                .namedParam(atom)
                .execute())
                .thenAccept(count -> {
                    LOGGER.log(Level.INFO, "Successfully saved new Atom: " + atom.name);
                    response.status(Http.Status.CREATED_201).send(atom);
                })
                .exceptionally(ex -> processErrors(ex, request, response));

    }

    /**
     * Update Atom object
     *
     * @param request  the server request
     * @param response the server response
     * @param atom     the Atom object from request body
     */

    private void updateAtomHandler(ServerRequest request, ServerResponse response, Atom atom) {

        String query = "{" +
                "\"collection\": \"atoms\"," +
                "\"query\":{atomNumber:$atomNumber}" +
                "\"value\": {" +
                "    $set: {\"name\": $name," +
                "           \"symbol\": $symbol," +
                "           \"mass\": $mass }" +
                "   }" +
                "}";

        String aNumber = request.path().param("atomNumber");
        int atomNumber = Integer.parseInt(aNumber);

        dbClient.execute(exec -> exec.createUpdate(query)
                .namedParam(atom)
                .execute())
                .thenAccept(count -> {
                    System.out.println(count);
                    LOGGER.log(Level.INFO, "Successfully Updated Atom: " + atom.name);
                    response.status(Http.Status.CREATED_201).send(atom);
                })
                .exceptionally(ex -> processErrors(ex, request, response));
    }

    /**
     * Deleting Atom with atom number = request param: atomNumber
     *
     * @param request  the server request
     * @param response the server response
     */
    private void deleteAtomHandler(ServerRequest request, ServerResponse response) {

        String query = "{" +
                "\"collection\": \"atoms\"," +
                "\"query\":{atomNumber:$atomNumber}" +
                "}";

        String aNumber = request.path().param("atomNumber");
        int atomNumber = Integer.parseInt(aNumber);

        dbClient.execute(exec -> exec.createDelete(query)
                .addParam("atomNumber", atomNumber)
                .execute())
                .thenAccept(count -> {
                    if(count == 0){
                        sendNotFoundError(response, aNumber);
                    }else {
                        LOGGER.log(Level.INFO, "Successfully deleted Atom with atomNumber: " + aNumber);
                        response.status(Http.Status.NO_CONTENT_204).send();
                    }
                })
                .exceptionally(ex -> processErrors(ex, request, response));
    }

    private <T> T processErrors(Throwable ex, ServerRequest request, ServerResponse response) {
        LOGGER.log(Level.WARNING, "Internal error", ex);
        ErrorResponse error = new ErrorResponse(500,
                "Error: " + ex.getMessage(),
                ex.getClass().getName(),
                LocalDateTime.now().toString());
        response.status(Http.Status.INTERNAL_SERVER_ERROR_500).send(error);

        return null;
    }
}
