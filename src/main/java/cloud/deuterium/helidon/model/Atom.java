package cloud.deuterium.helidon.model;

/**
 * Created by Milan Stojkovic 23-Apr-2021
 */
public class Atom {
    public int atomNumber;
    public String name;
    public String symbol;
    public double mass;

    public Atom() {
    }

    public Atom(int atomNumber, String name, String symbol, double mass) {
        this.atomNumber = atomNumber;
        this.name = name;
        this.symbol = symbol;
        this.mass = mass;
    }

    @Override
    public String toString() {
        return "Atom<" + name + ">";
    }
}
