package cloud.deuterium.helidon.mapper;

import cloud.deuterium.helidon.model.Atom;
import io.helidon.dbclient.DbMapper;
import io.helidon.dbclient.spi.DbMapperProvider;

import javax.annotation.Priority;
import java.util.Optional;

/**
 * Created by Milan Stojkovic 24-Apr-2021
 */

/**
 *  In resources/META-INF/services a file named: io.helidon.dbclient.spi.DbMapperProvider
 *  must be created,
 *  and: cloud.deuterium.helidon.mapper.AtomMapperProvider
 *  must be written
 *
 */
@Priority(1000)
public class AtomMapperProvider implements DbMapperProvider {

    private static final AtomMapper MAPPER = new AtomMapper();

    @SuppressWarnings("unchecked")
    @Override
    public <T> Optional<DbMapper<T>> mapper(Class<T> type) {
        if (type.equals(Atom.class)) {
            return Optional.of((DbMapper<T>) MAPPER);
        }
        return Optional.empty();
    }
}
