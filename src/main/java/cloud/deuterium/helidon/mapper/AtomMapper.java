package cloud.deuterium.helidon.mapper;

import cloud.deuterium.helidon.model.Atom;
import io.helidon.dbclient.DbColumn;
import io.helidon.dbclient.DbMapper;
import io.helidon.dbclient.DbRow;

import java.util.List;
import java.util.Map;

/**
 * Created by Milan Stojkovic 24-Apr-2021
 */
public class AtomMapper implements DbMapper<Atom> {
    @Override
    public Atom read(DbRow row) {
        DbColumn atomNumber = row.column("atomNumber");
        DbColumn name = row.column("name");
        DbColumn symbol = row.column("symbol");
        DbColumn mass = row.column("mass");
        return new Atom(atomNumber.as(Integer.class), name.as(String.class), symbol.as(String.class), mass.as(Double.class));
    }

    @Override
    public Map<String, Object> toNamedParameters(Atom value) {
        return Map.of("atomNumber", value.atomNumber,
                      "name", value.name,
                      "symbol", value.symbol,
                      "mass", value.mass);
    }

    @Override
    public List<?> toIndexedParameters(Atom value) {
        return List.of(value.atomNumber, value.name, value.symbol, value.mass);
    }
}
